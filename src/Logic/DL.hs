{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE FlexibleContexts #-}
-- | Description logic (ALC only at the moment)
module Logic.DL(
    -- * Types
    Concept(..),
    Role(..),
    Assertion(..),
    ABox,
    TableauState(..),
    -- * Satisfiability
    closed,
    open, 
    satisfiable,
    simpleSat
    ) where

import Control.Monad
import Control.Monad.Error
import Control.Monad.Identity
import Control.Monad.State.Lazy
import Data.Foldable hiding (all, any)
import Data.Maybe (listToMaybe, isJust)
import qualified Data.Set as S

-- | A concept
--   role type r
--   concept label d
data Concept r d =
    AConcept d
    | Neg (Concept r d)
    | Conj (Concept r d) (Concept r d)
    | Disj (Concept r d) (Concept r d)
    | Exists (Role r) (Concept r d)
    | Forall (Role r) (Concept r d)
    deriving (Eq, Ord, Show)

newtype Role r = Role { unRole :: r } deriving (Eq, Ord, Show)

data Assertion r d i = 
    ConceptAssertion i (Concept r d)
    | RoleAssertion  i i (Role r)
    deriving (Eq, Ord, Show)

type ABox r d i = S.Set (Assertion r d i)

allIndividuals :: Ord i => ABox r d i -> [i]
allIndividuals = S.toList . foldMap getI where
    getI (ConceptAssertion i _) = S.singleton i
    getI (RoleAssertion i1 i2 _) = S.fromList [i1,i2]

allConcepts :: (Ord r, Ord d) => ABox r d i -> [Concept r d]
allConcepts = S.toList . foldMap getD where
    getD (ConceptAssertion _ d) = S.singleton d
    getD (RoleAssertion _ _ _ ) = S.empty

toNNF :: Concept r d  -> Concept r d
toNNF c = case c of
    Neg (AConcept _) -> c
    Neg (Neg a) -> toNNF a
    Neg (Conj a b) -> Disj (propagate a) (propagate b)
    Neg (Disj a b) -> Conj (propagate a) (propagate b) 
    Neg (Exists r b) -> Forall r (propagate b)
    Neg (Forall r b) -> Exists r (propagate b)
    AConcept _ -> c
    Conj a b -> Conj (toNNF a) (toNNF b)
    Disj a b -> Disj (toNNF a) (toNNF b)
    Exists r b -> Exists r (toNNF b)
    Forall r b -> Forall r (toNNF b)
    where
        propagate = toNNF . Neg

makeNNF :: (Ord i, Ord d, Ord r) => ABox r d i -> ABox r d i
makeNNF = S.map go where
    go (ConceptAssertion i c) = ConceptAssertion i $ toNNF c
    go r                      = r

data TableauState i = T {
    individuals :: [i]
}

type TableauError = String

nextIndividual :: (MonadState (TableauState i) m, MonadError TableauError m) => m i
nextIndividual = do
    is <- get
    case (individuals is) of
        [] -> throwError "The list of individuals is empty"
        (x:xs) -> put is{individuals = xs} >> return x

data TransformationRule r d i = TR { 
    condition :: ABox r d i -> Maybe (Assertion r d i), 
    action :: (MonadState (TableauState i) m, MonadError TableauError m) => ABox r d i -> Assertion r d i -> m [ABox r d i] }

conjRule :: (Ord i, Ord d, Ord r) => TransformationRule r d i
conjRule = TR{..} where
    theFilter = S.filter . isConj
    condition as = listToMaybe $ S.toList $ theFilter as as
    isConj as (ConceptAssertion i (Conj a b)) = S.notMember (ConceptAssertion i a) as || S.notMember (ConceptAssertion i b) as
    isConj _ _ = False
    action as conj = case conj of
        (ConceptAssertion i (Conj a b)) -> return [S.insert (ConceptAssertion i a) $ S.insert (ConceptAssertion i b) $ as ]
        _ -> throwError "The conjunction rule needs to be called with a conjunction" 

disjRule :: (Ord i, Ord d, Ord r) => TransformationRule r d i
disjRule = TR{..} where
    theFilter = S.filter . isDisj
    condition as = listToMaybe $ S.toList $ theFilter as as
    isDisj as (ConceptAssertion i (Disj a b)) = S.notMember (ConceptAssertion i a) as && S.notMember (ConceptAssertion i b) as
    isDisj _ _ = False
    action as conj = case conj of
        (ConceptAssertion i (Disj a b)) -> return [S.insert (ConceptAssertion i a) as, S.insert (ConceptAssertion i b) as]
        _ -> throwError "The disjunction rule needs to be called with a disjunction"

existsRule :: (Ord i, Ord d, Ord r) => TransformationRule r d i
existsRule = TR{..} where
    theFilter = S.filter . isExists
    condition as = listToMaybe $ S.toList $ theFilter as as 
    isExists as (ConceptAssertion x (Exists r c)) = 
        let is = allIndividuals as in
        not $ any (\z -> (S.member (ConceptAssertion z c) as) && (S.member (RoleAssertion x z r) as)) is
    isExists _ _ = False
    action as ca = case ca of
        (ConceptAssertion x (Exists r c)) -> do 
            y <- nextIndividual
            return [S.insert (ConceptAssertion y c) $ S.insert (RoleAssertion x y r) $ as ]
        _ -> throwError "The exists rule needs to be called with an exists concept"
        
forallRule :: (Ord i, Ord d, Ord r) => TransformationRule r d i
forallRule = TR{..} where
    -- Expected type:  ABox r d i             -> Maybe (Assertion r d i)
    -- Actual type:    ABox r d (Concept r d) -> Maybe (Assertion r d (Concept r d))
    condition as = join $ listToMaybe $ S.toList $ S.map (getForalls as) as 
    getForalls :: (Ord i, Ord d, Ord r) => ABox r d i -> Assertion r d i -> Maybe (Assertion r d i)
    getForalls as a@(ConceptAssertion x (Forall r c)) = do
        y <- findY r x as
        guard (not $ S.member (ConceptAssertion y c) as)
        return a
    getForalls _ _ = Nothing
    findY :: (Ord i, Ord d, Ord r) => Role r -> i -> ABox r d i -> Maybe i
    findY role x = join . listToMaybe . S.toList . S.map (roleMp role x)
    roleMp role i (RoleAssertion i' i'' r) = if (i == i' && role == r) then (Just i'') else Nothing
    roleMp _    _ _                        = Nothing
    action as ca = case ca of
        (ConceptAssertion x (Forall r c)) ->
            let my = findY r x as in
            maybe (throwError "No y was found") (\y -> return $ [S.insert (ConceptAssertion y c) as]) my
        _ -> throwError "The forall rule needs to be called with a forall concept"

allRules :: (Ord i, Ord d, Ord r) => [TransformationRule r d i]
allRules = [conjRule, disjRule, existsRule, forallRule]

-- An Abox is complete if none of the transformation rules applies to it.
complete :: (Ord i, Ord d, Ord r) => ABox  r d i -> Bool
complete = not . isJust . ruleToApply

ruleToApply :: (Ord i, Ord d, Ord r) => ABox  r d i -> Maybe (TransformationRule r d i, Assertion r d i)
ruleToApply abox = join $ listToMaybe $ filter isJust $ fmap (\(r, a) -> maybe Nothing (\a' -> Just (r, a')) a) $ fmap (\r -> (r, condition r abox)) allRules

-- An Abox is closed if it contains P(x) and (\neg P(x)) for some individual x
-- closed == inconsistent
closed :: (Ord i, Ord d, Ord r) => ABox r d i -> Bool
closed abox = any (const $ True) conflicts where
    conflicts = [ (i, d) | 
        d <- allConcepts abox, 
        i <- allIndividuals abox, 
        S.member (ConceptAssertion i d) abox,
        S.member (ConceptAssertion i $ toNNF $ Neg d) abox ]

-- Open == consistent
open :: (Ord i, Ord d, Ord r) => ABox r d i -> Bool
open = not . closed

-- To determine whether an abox is satisfiable,
-- (1) check if any rules can be applied
-- (1a) If yes: Apply 1 rule
-- (2) check if all of the aboxes are closed (if yes, abort (unsatisfiable))
-- (3) Repeat for all open aboxes
satisfiable' :: (Functor m, MonadState (TableauState i) m, MonadError TableauError m, Ord d, Ord r, Ord i) => ABox r d i -> m Bool
satisfiable' abox = do
    let abox' = makeNNF abox
    if closed abox' 
    then return False
    else do
        let r = ruleToApply abox'
        maybe (return True) (\(r', a) -> action r' abox' a >>= \l' -> fmap (any ((==) True)) $ sequence $ fmap satisfiable' l') r
{-
    maybe (return $ open abox) go $ ruleToApply $ makeNNF abox where
    -- go applies the rule
    -- m0 [ABox r d i] -> t
    go (rule, assertion) = _ $ action rule abox assertion-}

satisfiable :: (Ord d, Ord r, Ord i) => TableauState i -> ABox r d i -> Either TableauError Bool
satisfiable st abox = runIdentity $ runErrorT (evalStateT (satisfiable' abox) st)

-- check satisfiability of a simple a box with string labels and
-- integer individuals
simpleSat :: ABox String String Integer -> Either TableauError Bool
simpleSat = satisfiable initialState where
    initialState = T [4..]
