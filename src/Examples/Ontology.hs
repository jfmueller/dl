module Examples.Ontology where

import qualified Data.Set as S
import Logic.DL

-- Some individuals
i1 :: Integer
i1 = 1

i2 :: Integer
i2 = 2

i3 :: Integer
i3 = 3

-- some concepts
thing :: Concept String String
thing = AConcept "thing"

animal :: Concept String String
animal = Exists (Role "eats") thing -- all animals eat something

person :: Concept String String
person = AConcept "person"

eats :: Integer -> Integer -> Assertion String String Integer
eats l r = RoleAssertion l r (Role "eats")

vegetarian :: Concept String String
vegetarian = Conj person noAnimals where
    noAnimals = Neg $ Exists (Role "eats") animal

vOntology :: ABox String String Integer 
vOntology = S.fromList [
    i1 `isA` thing,
    i1 `isA` animal,
    i1 `eats` i3,
    i3 `isA` thing,
    i3 `isA` animal,
    i2 `isA` person,
    i2 `isA` vegetarian,
    i2 `eats` i1
    ] where
        isA = ConceptAssertion

aBox1 :: ABox String String Integer
aBox1 = S.singleton $ ConceptAssertion i1 $ AConcept "Hello"

aBox2 :: ABox String String Integer
aBox2 = S.singleton $ ConceptAssertion i2 $ Neg $ AConcept "Hello"
