Implementation of description logics (DL) based on:

Franz Baader and Ulrike Sattler: An Overview of Tableau Algorithms for Description Logics. Studia Logica 69:1, pp. 5-40, October 2001.

At the moment it only supports ALC. 

Please see src/Examples for examples.

License: BSD3 (see LICENSE file)

Comments, contributions & bug reports are welcome!